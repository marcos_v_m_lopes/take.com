
class DashPage <SitePrism::Page
    section :nav, Sections::NavBar, 'div#onesignal-popover-container'
    
    element :globalMenu, 'div#header-fixed div > nav'
end
