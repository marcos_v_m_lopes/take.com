#!/usr/bin/ruby

class ReceitaNovaPage <SitePrism::Page
    set_url '/'

    section :nav, Sections::NavBar, '.global-menu'

    
    element :menuButon, 'button[class="menu-toggle"]'
    element :menuContato, '#menu-item-7'
    element :menuReceita, '#menu-item-27'

    element :nome, '#g2-name'
    element :email, '#g2-email'
    element :text, '#contact-form-comment-g2-comment'
    element :enviar, 'input[value="Enviar »"]'

    element :enviaido, '#contact-form-2'

    def acessoPagina()
        self.menuButon.click
        self.menuContato.click
    end

    def preencherCamposContato()
        self.nome.click
        self.nome.set "Teste"
        self.email.click
        self.email.set "teste@teste.com"
        self.text.click
        self.text.set "teste realizado para take.com em um novo processo para qa"
        sleep 5
        self.enviar.click
    end

    def acessoPaginaReceita()
        self.menuButon.click
        self.menuReceita.click
    end

end