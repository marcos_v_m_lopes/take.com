#language:pt

#Etapa 3 - Automatização do teste
Funcionalidade: qadatake
    Como usuario do site qadatake
    Gostaria de ter acesso a uma receita de bolo
    Para poder aprender como fazer um bolo

    Criterios de aceitação
        - Ao acessar a pagina https://qadatake.wordpress.com/2018/07/12/the-journey-begins/ devo ter acesso imediato a receita de bolo.
        - O site tem que ter um are de comentarios para o usuario poder dar sugestoes.
        - O site deve ter um campo de busca global.
        - O site deve ter uma pagina contendo todas as receitas de bolo ja cadastradas.
        - O site deve possui uma are de contato com o dono do site.


Cenario: enviar um contato
    
    Dado que eu acesse o site e entro na pagina de contato
    Quando preencho todos os campos e envio
    Então Consigo ter o retorno da mensagem enviada

Cenario: visualizar lista de receitas
    
    Dado que eu acesse o site e entro na pagina de receitas
    Então consigo ver todas as receitas cadastradas

