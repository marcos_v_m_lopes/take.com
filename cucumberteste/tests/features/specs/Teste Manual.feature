#language:pt

#Etapa 1 - Levantamento dos bugs

Funcionalidade: qadatake
    Como usuario do site qadatake
    Gostaria de ter acesso a uma receita de bolo
    Para poder aprender como fazer um bolo

    Criterios de aceitação
        - Ao acessar a pagina https://qadatake.wordpress.com/2018/07/12/the-journey-begins/ devo ter acesso imediato a receita de bolo.
        - O site tem que ter um are de comentarios para o usuario poder dar sugestoes.
        - O deve ter um campo de busca global.
        - O site deve ter uma pagina contendo todas as receitas de bolo ja cadastradas.
        - O site deve possui uma are de contato com o dono do site.

@bugs
    Título: 
        Acesso as receitas do site não esta carregando.

    Criticidade: 
        Auta

    Prioridade: 
        Auta

    Passo a passo para reproduzir o bug: 
        1 - Acesse a pagina https://qadatake.wordpress.com/2018/07/12/the-journey-begins/
        2 - clique em "menu" e logo em seguida clique em "Receitas"
        3 - Observe o erro ocorrendo.

    Resultado esperado:
        Ao acessar o menu de "Receita" o usuario podera ver em forma de lista todas as receitas ja cadastradas no site qadatake, ordenadas
        por tipo e categorias.


@melhorias
    * Ao clicar sobre a imagem do bolo, ele poderia apenas apresentar um zoom na imagem, como e o de costume em varios sites.
    * O acesso ao menu poderia ser mais didatico abrindo um especie de sanfona de menu contendo todos os acesso la, sem a nescessidade
    de mudar para outra pagina.
    * O sistema de pesquisa poderia ter um botão de "pesquisar", nem sempre apertar a tecla enter e intuitivo.


#Etapa 2 - Correção do Bug

    O bug a ser corrigido é o "Acesso as receitas do site não esta carregando." mediante a criticidade do erro, o sistema possui uma 
    pagina de receitas que e extremamente usual para o usuario final porem ela não esta carregando, impossibilitando o usuario de ter
    acesso a outras receitas.
