#encoding: utf-8

Dado(/^que eu acesse o site e entro na pagina de contato/) do
  receitaNova.load
  receitaNova.acessoPagina
end

Quando(/^preencho todos os campos e envio/) do
  receitaNova.preencherCamposContato
end

Então(/^Consigo ter o retorno da mensagem enviada/) do
  sleep 5
  expect(receitaNova.has_enviaido?).to be true
end


Dado(/^que eu acesse o site e entro na pagina de receitas/) do
  receitaNova.load
  receitaNova.acessoPaginaReceita
end

Então(/^consigo ver todas as receitas cadastradas/) do
  sleep 5
  expect(receitaNova.has_enviaido?).to be true
end